# coding: utf-8

# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = kpi_report_from_dict(json.loads(json_string))
from jinja2 import Template


def from_str(x):
    assert isinstance(x, (str, unicode))
    return x


def from_list(f, x):
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c, x):
    assert isinstance(x, c)
    return x.to_dict()


class KpiSet:
    def __init__(self, name, description, dimension, sub_dimension, category, unit, methodology, source, sdg):
        self.name = name
        self.description = description
        self.dimension = dimension
        self.sub_dimension = sub_dimension
        self.category = category
        self.unit = unit
        self.methodology = methodology
        self.source = source
        self.sdg = sdg

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        name = from_str(obj.get(u"name"))
        description = from_str(obj.get(u"description"))
        dimension = from_str(obj.get(u"dimension"))
        sub_dimension = from_str(obj.get(u"subDimension"))
        category = from_str(obj.get(u"category"))
        unit = from_str(obj.get(u"unit"))
        methodology = from_str(obj.get(u"methodology"))
        source = from_str(obj.get(u"source"))
        sdg = from_str(obj.get(u"sdg"))
        return KpiSet(name, description, dimension, sub_dimension, category, unit, methodology, source, sdg)

    def to_dict(self):
        result = {}
        result[u"name"] = from_str(self.name)
        result[u"description"] = from_str(self.description)
        result[u"dimension"] = from_str(self.dimension)
        result[u"subDimension"] = from_str(self.sub_dimension)
        result[u"category"] = from_str(self.category)
        result[u"unit"] = from_str(self.unit)
        result[u"methodology"] = from_str(self.methodology)
        result[u"source"] = from_str(self.source)
        result[u"sdg"] = from_str(self.sdg)
        return result


class KpiReport:
    def __init__(self, date, report_name, user_name, qr_code, logo, description, kpi_set):
        self.date = date
        self.report_name = report_name
        self.user_name = user_name
        self.qr_code = qr_code
        self.logo = logo
        self.description = description
        self.kpi_set = kpi_set

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        date = from_str(obj.get(u"date"))
        report_name = from_str(obj.get(u"reportName"))
        user_name = from_str(obj.get(u"userName "))
        qr_code = from_str(obj.get(u"qrCode"))
        logo = from_str(obj.get(u"logo"))
        description = from_str(obj.get(u"description"))
        kpi_set = from_list(KpiSet.from_dict, obj.get(u"kpiSet"))
        return KpiReport(date, report_name, user_name, qr_code, logo, description, kpi_set)

    def to_dict(self):
        result = {}
        result[u"date"] = from_str(self.date)
        result[u"reportName"] = from_str(self.report_name)
        result[u"userName "] = from_str(self.user_name)
        result[u"qrCode"] = from_str(self.qr_code)
        result[u"logo"] = from_str(self.logo)
        result[u"description"] = from_str(self.description)
        result[u"kpiSet"] = from_list(lambda x: to_class(KpiSet, x), self.kpi_set)
        return result


def kpi_report_from_dict(s):
    return KpiReport.from_dict(s)


def kpi_report_to_dict(x):
    return to_class(KpiReport, x)

def exportKpiReportFromDict(result):
    path = r'C:\Users\qosdesign\Desktop\smgis-extension\export\mdTemplates\kpi.tpl.md'
    with open(path, "r") as f:
        all = f.read()
    t = Template(all)

    rendered = t.render(R_E_P_O_R_T_N_A_M_E =result.report_name,
                        d_e_s_c_r_i_p_t_i_o_n=result.description,
                        listKpis=result.kpi_set)

    return rendered
if __name__ == "__main__":
    import json
    with open(r"C:\Users\qosdesign\Desktop\smgis-extension\export\jsonTemplates\kpiReport.json","r") as f:
        all = f.read()
    result = kpi_report_from_dict(json.loads(all))
    rendered = export(result)
    print rendered

