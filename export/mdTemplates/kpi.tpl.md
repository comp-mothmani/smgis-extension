#{{ R_E_P_O_R_T_N_A_M_E }}  

# Summary

    {{d_e_s_c_r_i_p_t_i_o_n}}

# KPIs

{% for n in listKpis %}

### KPI Name: {{n.name}}
         Description: {{n.description}}
        Dimension: {{n.dimension}}
        Sub-Dimension: {{n.sub_dimension}}
        Category: {{n.category}}
        Unit: {{n.unit}}
        Methodology: {{n.methodology}}
        Source: {{n.source}}
        Sdg: {{n.sdg}}
        
{% endfor %}
        



    



