# Benchmark Report : {{ CITY1 }} | {{ CITY2 }}

# Summary

    {{ d_e_s_c_r_i_p_t_i_o_n }}

# Parameters

| Parameter | {{ CITY1 }} | {{ CITY2 }} | DESCRIPTION | DATE |
| ------ | ------ | ------ | ------ | ------ | 
{% for n in valsList %} | {{ n.parameter }} | {{ n.city1_val }} | {{ n.city2_val }} | {{ n.description }} | {{ n.date }} |
{% endfor %}


# KPIS

{% for n in parsList %}

## KPI Name: {{n.name}}
         {{CITY1}} : {{n.city1_val}}
        {{CITY2}} : {{n.city2_val}}
        Description : {{n.description}}
        Date : {{n.date}}
        Dimension : {{n.dimension}}
        Sub-Dimension : {{n.sub_dimension}}
        Category : {{n.category}}
        Unit : {{n.unit}}
        Methodology : {{n.methodology}}
        Source : {{n.source}}
        Sdg : {{n.sdg}}
        
{% endfor %}