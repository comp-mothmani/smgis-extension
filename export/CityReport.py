# coding: utf-8

# To use this code, make sure you
#
#     import json
#
# and then, to convert JSON from a string, do
#
#     result = city_report_from_dict(json.loads(json_string))
from jinja2 import Template


def from_str(x):
    assert isinstance(x, (str, unicode))
    return x


def from_list(f, x):
    assert isinstance(x, list)
    return [f(y) for y in x]


def to_class(c, x):
    assert isinstance(x, c)
    return x.to_dict()


class ChartsList:
    def __init__(self, city1_name, city2_name, name, kpi_name, img):
        self.city1_name = city1_name
        self.city2_name = city2_name
        self.name = name
        self.kpi_name = kpi_name
        self.img = img

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        city1_name = from_str(obj.get(u"city1Name"))
        city2_name = from_str(obj.get(u"city2Name"))
        name = from_str(obj.get(u"name"))
        kpi_name = from_str(obj.get(u"kpiName"))
        img = from_str(obj.get(u"img"))
        return ChartsList(city1_name, city2_name, name, kpi_name, img)

    def to_dict(self):
        result = {}
        result[u"city1Name"] = from_str(self.city1_name)
        result[u"city2Name"] = from_str(self.city2_name)
        result[u"name"] = from_str(self.name)
        result[u"kpiName"] = from_str(self.kpi_name)
        result[u"img"] = from_str(self.img)
        return result


class KpiSet:
    def __init__(self, value, name, description, date, dimension, sub_dimension, category, unit, methodology, source, sdg):
        self.value = value
        self.name = name
        self.description = description
        self.date = date
        self.dimension = dimension
        self.sub_dimension = sub_dimension
        self.category = category
        self.unit = unit
        self.methodology = methodology
        self.source = source
        self.sdg = sdg

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        value = int(from_str(obj.get(u"value")))
        name = from_str(obj.get(u"name"))
        description = from_str(obj.get(u"description"))
        date = from_str(obj.get(u"date"))
        dimension = from_str(obj.get(u"dimension"))
        sub_dimension = from_str(obj.get(u"subDimension"))
        category = from_str(obj.get(u"category"))
        unit = from_str(obj.get(u"unit"))
        methodology = from_str(obj.get(u"methodology"))
        source = from_str(obj.get(u"source"))
        sdg = from_str(obj.get(u"sdg"))
        return KpiSet(value, name, description, date, dimension, sub_dimension, category, unit, methodology, source, sdg)

    def to_dict(self):
        result = {}
        result[u"value"] = from_str(str(self.value))
        result[u"name"] = from_str(self.name)
        result[u"description"] = from_str(self.description)
        result[u"date"] = from_str(self.date)
        result[u"dimension"] = from_str(self.dimension)
        result[u"subDimension"] = from_str(self.sub_dimension)
        result[u"category"] = from_str(self.category)
        result[u"unit"] = from_str(self.unit)
        result[u"methodology"] = from_str(self.methodology)
        result[u"source"] = from_str(self.source)
        result[u"sdg"] = from_str(self.sdg)
        return result


class PSet:
    def __init__(self, parameter, value, description, date):
        self.parameter = parameter
        self.value = value
        self.description = description
        self.date = date

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        parameter = from_str(obj.get(u"parameter"))
        value = int(from_str(obj.get(u"value")))
        description = from_str(obj.get(u"description"))
        date = from_str(obj.get(u"date"))
        return PSet(parameter, value, description, date)

    def to_dict(self):
        result = {}
        result[u"parameter"] = from_str(self.parameter)
        result[u"value"] = from_str(str(self.value))
        result[u"description"] = from_str(self.description)
        result[u"date"] = from_str(self.date)
        return result


class CityReport:
    def __init__(self, date, report_name, user_name, qr_code, logo, city_name, description, kpi_set, p_set, charts_list):
        self.date = date
        self.report_name = report_name
        self.user_name = user_name
        self.qr_code = qr_code
        self.logo = logo
        self.city_name = city_name
        self.description = description
        self.kpi_set = kpi_set
        self.p_set = p_set
        self.charts_list = charts_list

    @staticmethod
    def from_dict(obj):
        assert isinstance(obj, dict)
        date = from_str(obj.get(u"date"))
        report_name = from_str(obj.get(u"reportName"))
        user_name = from_str(obj.get(u"userName "))
        qr_code = from_str(obj.get(u"qrCode"))
        logo = from_str(obj.get(u"logo"))
        city_name = from_str(obj.get(u"cityName"))
        description = from_str(obj.get(u"description"))
        kpi_set = from_list(KpiSet.from_dict, obj.get(u"kpiSet"))
        p_set = from_list(PSet.from_dict, obj.get(u"pSet"))
        charts_list = from_list(ChartsList.from_dict, obj.get(u"chartsList"))
        return CityReport(date, report_name, user_name, qr_code, logo, city_name, description, kpi_set, p_set, charts_list)

    def to_dict(self):
        result = {}
        result[u"date"] = from_str(self.date)
        result[u"reportName"] = from_str(self.report_name)
        result[u"userName "] = from_str(self.user_name)
        result[u"qrCode"] = from_str(self.qr_code)
        result[u"logo"] = from_str(self.logo)
        result[u"cityName"] = from_str(self.city_name)
        result[u"description"] = from_str(self.description)
        result[u"kpiSet"] = from_list(lambda x: to_class(KpiSet, x), self.kpi_set)
        result[u"pSet"] = from_list(lambda x: to_class(PSet, x), self.p_set)
        result[u"chartsList"] = from_list(lambda x: to_class(ChartsList, x), self.charts_list)
        return result


def city_report_from_dict(s):
    return CityReport.from_dict(s)


def city_report_to_dict(x):
    return to_class(CityReport, x)



def exportCityReportFromDict(result):
    path = r'C:\Users\qosdesign\Desktop\smgis-extension\export\mdTemplates\city.tpl.md'
    with open(path, "r") as f:
        all = f.read()
    t = Template(all)

    rendered = t.render(d_e_s_c_r_i_p_t_i_o_n=result.description,
                        c_i_t_y_n_a_m_e = result.city_name,
                        valsList=result.kpi_set,
                        parsList=result.p_set)

    return rendered
if __name__ == "__main__":
    import json
    with open(r"C:\Users\qosdesign\Desktop\smgis-extension\export\jsonTemplates\cityReport.json","r") as f:
        all = f.read()
    result = city_report_from_dict(json.loads(all))
    rendered = export(result)
    #print rendered

    with open(r"C:\Users\qosdesign\Desktop\smgis-extension\export\rendered\renderedCityReport.md","w") as f:
        f.write(rendered)