import base64
import os
import shapefile
import shutil

from cfg import SHP_EXPORT_PATH


def export2shp(data):
    try:
        from StringIO import StringIO
    except ImportError:
        from io import BytesIO as StringIO

    shp = StringIO()
    shx = StringIO()
    dbf = StringIO()
    w = shapefile.Writer(shp=shp, shx=shx, dbf=dbf, shapeType=3)
    w.field('props', fieldType='C', size=100)

    for i in data:
        w.line([list(i.getGeometry().toListInv())])
        w.record("ddd")
    w.close()

    with open(SHP_EXPORT_PATH + "/shp_export_2d.shp", "w") as f:
        f.write(shp.getvalue())

    with open(SHP_EXPORT_PATH + "/shp_export_2d.shx", "w") as f:
        f.write(shx.getvalue())

    with open(SHP_EXPORT_PATH + "/shp_export_2d.dbf", "w") as f:
        f.write(dbf.getvalue())

    try:
        os.remove(SHP_EXPORT_PATH + "/shp_export.zip")
    except:
        pass
    shutil.make_archive(SHP_EXPORT_PATH + "/shp_export", 'zip', SHP_EXPORT_PATH + "/")

    with open(SHP_EXPORT_PATH + "/shp_export.zip", "rb") as f:
        bytes = f.read()
        encoded = base64.b64encode(bytes)
    return encoded