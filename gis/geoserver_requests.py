from geoserver.catalog import Catalog

from cfg import GEOSERVER_USERNAME, GEOSERVER_PASSWORD


def requestLayers():
    cat = Catalog("http://localhost:8080/geoserver/rest",
                  username=GEOSERVER_USERNAME,
                  password=GEOSERVER_PASSWORD)
    layers = cat.get_layers()
    lays = []
    for i in layers:
        if i.name.startswith("CERT"):
            lays.append(i.name)
            print i.catalog.__dict__
    return lays

def requestGroupLayers():
    cat = Catalog("http://localhost:8080/geoserver/rest",
                  username=GEOSERVER_USERNAME,
                  password=GEOSERVER_PASSWORD)
    layers = cat.get_layergroups(workspace="CERT")
    lays = []
    for i in layers:
        print i.layers
    return lays


def requestStores():
    cat = Catalog("http://localhost:8080/geoserver/rest",
                  username=GEOSERVER_USERNAME,
                  password=GEOSERVER_PASSWORD)
    store = cat.get_store("	tunisia","CERT")
    print store.__dict__


if __name__ == "__main__":
    requestGroupLayers()
