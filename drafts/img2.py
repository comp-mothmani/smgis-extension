import os
from os import listdir
from os.path import isfile, join
import cv2
import numpy as np


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

mypath = r'/home/medzied/PycharmProjects/smgis-extension/drafts/15adace2-8619-11e9-9d71-0cc47a792c0a_id_15adace2-8619-11e9-9d71-0cc47a792c0a_files'
mypath1 = r'/home/medzied/PycharmProjects/smgis-extension/drafts/processed'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()


for i in onlyfiles[:50]:
    original = cv2.imread(mypath + "/" + i)
    cv2.imwrite(mypath1 + "/" + i, original)

    rotated = rotate_bound(original, -90)
    (h, w) = rotated.shape[:2]
    y = 70
    h = 130
    x = 0

    crop_img = rotated[y:y + h, x:x + w]
    crop_img_hsv = cv2.cvtColor(crop_img, cv2.COLOR_BGR2RGB)

    crop_img_black = crop_img_hsv.copy()
    crop_img_black[:, :, 0] = 0
    crop_img_black[:, :, 1] = 0
    ''''
    edges = cv2.Canny(crop_img_black,100,200)
    
    ret, thresh = cv2.threshold(edges, 127, 255, 0)
    im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    imgray = cv2.cvtColor(crop_img_hsv, cv2.COLOR_BGR2GRAY)
    ret, thresh = cv2.threshold(imgray, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    crop_img_black[:, :, 2] = 0
    #cv2.drawContours(crop_img_black, contours, -1, (255, 255, 255), 1)
    cv2.fillPoly(crop_img_black, pts=contours, color=(255, 255, 255))
    '''
    cv2.imwrite(mypath1 + "/" + i, crop_img)
