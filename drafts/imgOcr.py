from os import listdir

from os.path import isfile, join

try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract



mypath = r'/home/medzied/PycharmProjects/smgis-extension/drafts/processed'
mypath1 = r'/home/medzied/PycharmProjects/smgis-extension/drafts/processed'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()


for i in onlyfiles[:50]:
    # If you don't have tesseract executable in your PATH, include the following:

    # Simple image to string
    print '####'
    s = pytesseract.image_to_string(Image.open(mypath + "/" + i), lang='fra')
    print s
