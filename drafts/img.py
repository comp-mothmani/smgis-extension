import os
from os import listdir
from os.path import isfile, join
import cv2
import numpy as np


mypath = r'/home/medzied/PycharmProjects/smgis-extension/drafts/15adace2-8619-11e9-9d71-0cc47a792c0a_id_15adace2-8619-11e9-9d71-0cc47a792c0a_files'
mypath1 = r'/home/medzied/PycharmProjects/smgis-extension/drafts/processed'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()


def rotate_bound(image, angle):
    # grab the dimensions of the image and then determine the
    # center
    (h, w) = image.shape[:2]
    (cX, cY) = (w // 2, h // 2)

    # grab the rotation matrix (applying the negative of the
    # angle to rotate clockwise), then grab the sine and cosine
    # (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), -angle, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # compute the new bounding dimensions of the image
    nW = int((h * sin) + (w * cos))
    nH = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (nW / 2) - cX
    M[1, 2] += (nH / 2) - cY

    # perform the actual rotation and return the image
    return cv2.warpAffine(image, M, (nW, nH))

for i in onlyfiles[:50]:
    img1 = cv2.imread(mypath + "/" + i)
    img = cv2.imread(mypath + "/" + i,0)
    rows, cols = img.shape

    '''
    res = cv2.resize(img, None, fx=0.5, fy=0.5, interpolation=cv2.INTER_CUBIC)
    rows, cols = res.shape
    M = cv2.getRotationMatrix2D((cols / 2, rows / 2), 90, 1)
    dst = cv2.warpAffine(res, M, (cols, rows))
    '''

    dst = rotate_bound(img1,-90)
    (h, w) = dst.shape[:2]
    y = 70
    h = 130
    x = 0

    crop_img = dst[y:y + h, x:x + w]
    #ret, thresh1 = cv2.threshold(crop_img, 500, 40, cv2.CALIB_CB_ADAPTIVE_THRESH)
    light_grey = (141,141,141)
    dark_grey = (76,76,76)
    hsv_nemo = cv2.cvtColor(crop_img, cv2.COLOR_RGB2HSV)
    mask = cv2.inRange(hsv_nemo, light_grey, dark_grey)
    result = cv2.bitwise_and(crop_img, hsv_nemo, mask=mask)


    cv2.imwrite(mypath1 + "/" + i, result)

