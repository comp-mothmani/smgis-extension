import re

with open("draft.txt","r") as f:
    all = f.read()


k = 0
for i in all.split("Delegation")[2:-1]:
    with open("raw/entry" + str(k) + ".csv", "w") as f:
        for j in i.split("#"):
            s = ";".join(j.split("\n")[:-3]).replace(",",".").replace(";",",") + "\n"
            s = re.sub(r'[^\x00-\x7f]', r'', s)
            f.write(s)

    k += 1