#-*-coding:utf8;-*-
#qpy:console


import os
from os import listdir
from os.path import isfile, join

def read(p):
    with open(p,"r") as f:
        return f.read()
def meanCol(s):
    c = 0
    d =[]
    for i in s.split("\n")[1:]:
        d.append(len(i.split(",")))
    f = {x:d.count(x) for x in d}
    m = max(f.values())
    return f.keys()[f.values().index(m)]

#s = os.path.realpath(_file_)
#mypath = s[:-8] + "/raw"

mypath = r'/home/medzied/PycharmProjects/smgis-extension/drafts/raw'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
onlyfiles.sort()
k = 0
for i in onlyfiles:
    all = read(mypath + "/" +i).split("Total Tunisie")[0]
    col = meanCol(all)
    d =[]
    for j in all.split("\n"):
        d.append(j.split(",")[:col])
    with open("final/" + i ,"w") as f:
        for j in d:
            f.write(';'.join(j) + '\n')
    k +=1