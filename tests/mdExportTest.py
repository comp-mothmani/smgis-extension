# -*- coding: utf-8 -*-
"""
This is a test script.
This script will test te route /api
"""
import requests
import time

# Start Test
start = time.time()

# URL
url = "http://localhost:5000/exportKpiReport"
print "URL : " + url

payload = {

    "data" : open("C:\Users\qosdesign\Desktop\smgis-extension\export\jsonTemplates\kpiReport.json","r").read()

}
r = requests.post(url, data=payload).content

print "Response: {}".format(r)


done = time.time()
elapsed = done - start
print "This test was done in ", elapsed, "seconds"