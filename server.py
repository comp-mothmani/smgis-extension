import flask
from flask import Flask, request, json

from export.CityReport import city_report_from_dict, exportCityReportFromDict
from export.KpiReport import kpi_report_from_dict, exportKpiReportFromDict
from export.BenchmarkReport import  benchmark_report_from_dict, export
from gis.exportShapefile import export2shp
from gis.geoserver_requests import requestLayers

app = Flask(__name__, static_url_path='/assets', static_folder='assets')

@app.route('/exportKpiReport', methods=['GET', 'POST'])
def exportKpiReport():
    data = request.form['data']
    result = kpi_report_from_dict(json.loads(data))
    rendered = exportKpiReportFromDict(result)
    return flask.jsonify({"md" : rendered})

@app.route('/exportCityReport', methods=['GET', 'POST'])
def exportCityReport():
    data = request.form['data']
    result = city_report_from_dict(json.loads(data))
    rendered = exportCityReportFromDict(result)
    return flask.jsonify({"md" : rendered})

@app.route('/exportBenchmarkReport', methods=['GET', 'POST'])
def exportBenchmarkReport():
    data = request.form['data']
    result = benchmark_report_from_dict(json.loads(data))
    rendered = export(result)
    return flask.jsonify({"md" : rendered})

@app.route('/export_shp', methods=['GET', 'POST'])
def export2shp():
    data = request.form['data']
    data_json = data.decode('utf-8').replace('\0', '')
    struct = json.loads(data_json)

    ss = export2shp(struct)

    return flask.jsonify(ss)

@app.route('/geoserver_layers', methods=['GET', 'POST'])
def getLayers():
    return flask.jsonify(requestLayers())


if __name__ == "__main__":
    app.run('127.0.0.1', 5000, threaded=True, debug=True)